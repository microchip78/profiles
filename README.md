# Profiles

Please find a **[demo](http://profiles.kanchi.rocks)** of this application **[here](http://profiles.kanchi.rocks)**

*Note: As this demo is running on **Non SSL Server**, it will not load browser's current location on create new profile page. **Browser's Location API** only accessed from trusted websites only*



## Prerequisite

Make sure you have **node**, **npm** and **@angular/cli** globally installed

------

To check **node** installed, run on console

``` bash
node -v
```

console output will be something like this with **node** version no

```bash
v9.5.0
```

-------

To check **npm** installed, run on console

```bash
npm -v
```

console output will be something like this with **npm** version no

```bash
5.6.0
```

----

To check **@angular/cli** installed, run on console

```bash
ng -v
```

console output will be something like this with **@angular/cli** global version installed

```bash

    _                      _                 ____ _     ___
   / \   _ __   __ _ _   _| | __ _ _ __     / ___| |   |_ _|
  / △ \ | '_ \ / _` | | | | |/ _` | '__|   | |   | |    | |
 / ___ \| | | | (_| | |_| | | (_| | |      | |___| |___ | |
/_/   \_\_| |_|\__, |\__,_|_|\__,_|_|       \____|_____|___|
               |___/
    
Angular CLI: 1.7.1
Node: 9.5.0
OS: darwin x64
Angular: 
...

```

if you dont have **@angular/cli** installged globally, please install **@angular/cli** running following command in your console

```bash
npm install -g @angular/cli@latest
```

**Note: If you are installing on *mac* you may come across an issue with permission, if thats the case use *nvm* or install node with *homebrew* **

-------

Once you have **node**, **npm**, **@angular/cli** installed globally, you are ready to build and run any angular project locally



## Clone Profiles

Navigate to **~** and **clone** repository

```bash
cd ~
git clone git@bitbucket.org:microchip78/profiles.git
```



------



## Install dependencies for profiles

Get in to project directory and install **dependencies**

```bash
cd ~/profiles
npm install
```



-------



## Run Profiles

```bash
cd ~/profiles
ng serve
```

Open your favourite browser (Tested on **Google Chrome** and **Firefox** only) and browse to http://localhost:4200 to see the **Profiles** working



Run **Profiles** on different port

```bash
cd ~/profiles
ng serve --port 4000
```

If you are using different port to run **Profiles** please use that port when you are browsing to locally running website e.g. http://localhost:4000



-------



## Build Profiles

```bash
cd ~/profiles
ng build --prod
```

This will produce production ready build in **~/profiles/dist** folder. You can slimply take it and move to root folder of your **statically** hosted website



------



## Demo Profiles

Please find a **[demo](http://profiles.kanchi.rocks)** of this application **[here](http://profiles.kanchi.rocks)**

*Note: As this demo is running on **Non SSL Server**, it will not load browser's current location on create new profile page. **Browser's Location API** only accessed from trusted websites only*



 