import { Injectable, Optional } from '@angular/core';
import { LocalStorageConfig } from './local-store.config';

function getWindow (): any {
	return window;
}

@Injectable()
export class LocalStorageService {
	private config: LocalStorageConfig = <LocalStorageConfig>{};

	private storage: any;
	private window: Window;
	private _localStorageSupported: boolean = false;

	public get localStorageSupported() {
		return this._localStorageSupported;
	};

	constructor(@Optional() config: LocalStorageConfig) {
		if (config) {
			this.config = config;
		}
		this.window = getWindow();
		this._localStorageSupported = typeof(Storage) !== 'undefined';
		if (this._localStorageSupported) {
			this.storage = this.window.sessionStorage;
			if (this.config.storageType == 'local') {
				this.storage = this.window.localStorage;
			}
		}
	}

	public set(key: string, object: any): boolean {
		if (this._localStorageSupported) {
			let objectType = typeof object;
			this.storage.setItem(key + '-type', objectType);
			if (objectType === 'object') {
				object = JSON.stringify(object);
			}
			this.storage.setItem(key, object);
			return true;
		} else {
			return false;
		}
	}

	public get(key: string) {
		if (this._localStorageSupported) {
			let objectType = this.storage.getItem(key + '-type');
			let retObj = this.storage.getItem(key);
			if (retObj && retObj !== 'undefined') {
				if (objectType === 'object') {
					retObj = JSON.parse(retObj);
				}
				return retObj;
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	public delete(key: string) {
		if (this._localStorageSupported) {
			this.storage.removeItem(key);
			return true;
		} else {
			return false;
		}
	}}