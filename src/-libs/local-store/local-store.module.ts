import { ModuleWithProviders, NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LocalStorageConfig } from './local-store.config';
import { LocalStorageService } from './local-storage.service';

@NgModule({
	imports: [
		CommonModule
	],
	providers: [
		LocalStorageService
	]
})
export class LocalStoreModule {

	constructor (@Optional() @SkipSelf() parentModule: LocalStoreModule) {
		if (parentModule) {
			throw new Error('WebApiModule is already loaded. Import it in the AppModule only');
		}
	}

	static forRoot(config: LocalStorageConfig): ModuleWithProviders {
		return {
			ngModule: LocalStoreModule,
			providers: [
				{ provide: LocalStorageConfig, useValue: config }
			]
		};
	}
}
