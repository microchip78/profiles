import { LOCALE_ID, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
	MatButtonModule,
	MatCardModule,
	MatCheckboxModule,
	MatIconModule,
	MatInputModule,
	MatSidenavModule,
	MatToolbarModule,
	MatMenuModule,
	MatNativeDateModule,
	MatDatepickerModule,
	MatDialogModule,
	MatSelectModule,
	MatSlideToggleModule,
	MatButtonToggleModule,
	MatRadioModule,
	MatAutocompleteModule,
	MatSliderModule,
	MatProgressSpinnerModule,
	MatPaginatorModule,
	MatTableModule,
	MatProgressBarModule
} from '@angular/material';

@NgModule({
	imports: [
		CommonModule,
		BrowserAnimationsModule,
		MatButtonModule,
		MatCardModule,
		MatCheckboxModule,
		MatIconModule,
		MatInputModule,
		MatSidenavModule,
		MatToolbarModule,
		MatMenuModule,
		MatDialogModule,
		MatNativeDateModule,
		MatDatepickerModule,
		MatSelectModule,
		MatSlideToggleModule,
		MatButtonToggleModule,
		MatRadioModule,
		MatAutocompleteModule,
		MatSliderModule,
		MatProgressSpinnerModule,
		MatProgressBarModule,
		MatTableModule,
		MatPaginatorModule
	],
	declarations: [
	],
	providers: [
		{ provide: LOCALE_ID, useValue: 'en-AU' }
	],
	exports: [
		MatButtonModule,
		MatCardModule,
		MatCheckboxModule,
		MatIconModule,
		MatInputModule,
		MatSidenavModule,
		MatToolbarModule,
		MatMenuModule,
		MatDialogModule,
		MatNativeDateModule,
		MatDatepickerModule,
		MatSelectModule,
		MatSlideToggleModule,
		MatButtonToggleModule,
		MatRadioModule,
		MatAutocompleteModule,
		MatSliderModule,
		MatProgressSpinnerModule,
		MatProgressBarModule,
		MatTableModule,
		MatPaginatorModule
	]
})
export class MaterialDesignModule {
}
