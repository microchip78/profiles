import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LocalStoreModule } from '../../../-libs/local-store/local-store.module';
import { ProfileService } from './services/profile.service';
import { ProfileGuard } from './services/profile.guard';

@NgModule({
	imports: [
		CommonModule,
		LocalStoreModule.forRoot({
			storageType: 'local'
		})
	],
	declarations: [],
	providers: [
		ProfileService,
		ProfileGuard
	]
})
export class CoreModule {
}
