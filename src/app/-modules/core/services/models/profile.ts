export interface Profile {
	id: string;
	name: string;
	email: string;
	dateOfBirth: Date;
	location: string;
	geoLocation: GeoLocation;
}

export interface GeoLocation {
	lat: number;
	lng: number;
}
