import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { ProfileService } from './profile.service';

@Injectable()
export class ProfileGuard implements CanActivate {

	constructor(private profileService: ProfileService, private router: Router) {}

	canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
		const profileId = next.params['id'];
		if (this.profileService.profileExists(profileId)) {
			return true;
		} else {
			window.alert(`Profile '${profileId}' not found in system, routing back to profiles page`);
			this.router.navigate(['/']);
			return false;
		}
	}
}
