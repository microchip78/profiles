import { Injectable } from '@angular/core';
import { LocalStorageService } from '../../../../-libs/local-store/local-storage.service';
import { UUID } from 'angular2-uuid';
import { Profile } from './models/profile';

@Injectable()
export class ProfileService {

	private localStorageKey = 'x-profiles';
	private profiles: Profile[] = [];

	constructor(private localStore: LocalStorageService) {}

	public allProfiles(): Profile[] {
		this.loadProfiles();
		return this.profiles;
	}

	public saveProfiles() {
		this.localStore.set(this.localStorageKey, this.profiles);
	}

	public loadProfiles() {
		this.profiles = (this.localStore.get(this.localStorageKey) || []);
	}

	public addProfile(profile: Profile): boolean {
		profile.id = UUID.UUID();
		this.profiles.push(profile);
		this.saveProfiles();
		return true;
	}

	public profileExists(profileId: string): boolean {
		this.loadProfiles();
		const foundProfile = this.profiles.filter(p => p.id === profileId)[0] || null;
		return !!foundProfile;
	}

	public updateProfile(profile: Profile): boolean {
		this.loadProfiles();
		let profileIndex = -1;
		console.log('updated profile : ', profile);

		this.profiles.forEach((p, i) => {
			if (p.id === profile.id) {
				profileIndex = i;
				return;
			}
		});

		if (profileIndex >= 0) {
			this.profiles[profileIndex] = profile;
			this.saveProfiles();
			return true;
		} else {
			return false;
		}
	}

	public getProfile(profileId: string): Profile {
		this.loadProfiles();
		return this.profiles.filter(p => p.id === profileId)[0] || null;
	}

	public deleteProfile(profileId: string): boolean {
		if (this.profileExists(profileId)) {
			this.profiles = this.profiles.filter(p => p.id !== profileId);
			this.saveProfiles();
			return true;
		} else {
			return false;
		}
	}
}
