export interface MapClickedEvent {
	coords: MapCoordinates;
}

interface MapCoordinates {
	lat: number;
	lng: number;
}

// just an interface for type safety.
export interface Marker {
	lat: number;
	lng: number;
	label?: string;
	draggable: boolean;
}

