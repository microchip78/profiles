import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
	name: 'moment'
})
export class MomentPipe implements PipeTransform {

	transform(value: string|Date, conversionFormat: string): any {
		if (typeof value === 'string' || value instanceof Date) {
			return moment(value).format(conversionFormat);
		}
		return value;
	}
}
