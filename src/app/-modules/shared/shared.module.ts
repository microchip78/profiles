import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AgmCoreModule } from '@agm/core';
import { MomentPipe } from './pipes/moment.pipe';
import { HttpClientModule } from '@angular/common/http';
import { GoogleGeoCodingService } from './services/google-geo-coding.service';

@NgModule({
	imports: [
		CommonModule,
		HttpClientModule,
		AgmCoreModule.forRoot({
			apiKey: 'AIzaSyAOdUCXG8k668TZL0jaPRVyt_-Pa9BeyXY'
		}),
	],
	declarations: [
		MomentPipe
	],
	providers: [
		GoogleGeoCodingService
	],
	exports: [
		MomentPipe
	]
})
export class SharedModule {
}
