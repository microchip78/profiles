import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { HttpClient } from '@angular/common/http';
import { GeocoderResult } from './models/GeocoderResult';
import { map } from 'rxjs/operators';

@Injectable()
export class GoogleGeoCodingService {
	private addressLookupUrl = 'https://maps.googleapis.com/maps/api/geocode/json?latlng={lat},{lng}&key=AIzaSyAOdUCXG8k668TZL0jaPRVyt_-Pa9BeyXY';
	constructor(private http: HttpClient) {}

	private addressLookup(lat: number, lng: number): Observable<GeocoderResult[]> {
		const url = this.addressLookupUrl.replace('{lat}', lat.toString()).replace('{lng}', lng.toString());
		return this.http.get(url).pipe(
			map((response: any) => {
				if (response.status === 'OK') {
					return response.results;
				} else {
					return [];
				}
			})
		);
	}

	public getSuburb(lat: number, lng: number): Observable<string> {
		return this.addressLookup(lat, lng).pipe(
			map((results: GeocoderResult[]) => {
				return results[0].address_components[results[0].address_components.length - 5].short_name;
			})
		);
	}

}
