export interface GeocoderResult {
	address_components: GeocoderAddressComponent[];
	formatted_address: string;
	geometry: GeocoderGeometry;
	partial_match: boolean;
	place_id: string;
	postcode_localities: string[];
	types: string[];
}

export interface GeocoderAddressComponent {
	long_name: string;
	short_name: string;
	types: string[];
}

export interface GeocoderGeometry {
	bounds: GeocoderBouds;
	location: GeocoderLocation;
	location_type: GeocoderLocationType;
	viewport: GeocoderViewport;
}

export interface GeocoderBouds {
}

export enum GeocoderLocationType {
	APPROXIMATE,
	GEOMETRIC_CENTER,
	RANGE_INTERPOLATED,
	ROOFTOP
}

export interface GeocoderViewport {
	northeast: GeocoderLocation;
	southwest: GeocoderLocation;
}

export interface GeocoderLocation {
	lat: number;
	lng: number;
}

