import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { ProfileComponent } from './profile/profile.component';
import { AppRoutesModule } from './app.routes';
import { ProfilesComponent } from './profiles/profiles.component';
import { CoreModule } from './-modules/core/core.module';
import { SharedModule } from './-modules/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialDesignModule } from './-modules/material-design/material-design.module';
import { AgmCoreModule } from '@agm/core';

@NgModule({
	imports: [
		BrowserModule,
		CoreModule,
		SharedModule,
		FormsModule,
		ReactiveFormsModule,
		MaterialDesignModule,
		AgmCoreModule.forRoot({
			apiKey: "AIzaSyAOdUCXG8k668TZL0jaPRVyt_-Pa9BeyXY"
		}),
		AppRoutesModule
	],
	declarations: [
		AppComponent,
		ProfileComponent,
		ProfilesComponent
	],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule {
}
