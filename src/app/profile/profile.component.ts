import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GeoLocation, Profile } from '../-modules/core/services/models/profile';
import { Subscription } from 'rxjs/Subscription';
import { ProfileService } from '../-modules/core/services/profile.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GoogleGeoCodingService } from '../-modules/shared/services/google-geo-coding.service';
import { MapClickedEvent, Marker } from '../-modules/core/models/MapClickedEvent';


@Component({
	selector: 'app-profile',
	templateUrl: './profile.component.html',
	styleUrls: ['./profile.component.scss']
})
export class ProfileComponent {

	private isNew = false;
	public profile: Profile;
	private routeSub: Subscription;
	public profileId: string;

	public profileForm: FormGroup;

	public marker = <Marker>{
		lat: -37.811411388625636,
		lng: 144.96541500091553,
		label: 'Melbourne',
		draggable: true
	};

	public zoom = 12;

	public lat = -37.811411388625636;
	public lng = 144.96541500091553;

	constructor(
		private route: ActivatedRoute,
		private router: Router,
		private profileService: ProfileService,
		private geoCodingService: GoogleGeoCodingService,
		private fb: FormBuilder) {

		this.isNew = (this.route.snapshot.data[0] || {}).isNew || false;

		if (this.isNew) {
			this.setGeoLocation();
		}

		this.routeSub = this.route.params.subscribe(params => {
			this.profileId = params['id'];
			if (!this.isNew) {
				this.loadProfile();
			}
		});
	}

	private setGeoLocation() {
		this.createForm();
		if (navigator.geolocation) {
			const options = {
				enableHighAccuracy: true
			};

			navigator.geolocation.getCurrentPosition(position => {
				this.setMapAndMarker(position.coords.latitude, position.coords.longitude);
			}, error => {
				console.log(error);
			}, options);
		}
	}

	private setMapAndMarker(lat: number, lng: number) {
		this.lat = lat;
		this.lng = lng;

		this.geoCodingService.getSuburb(this.lat, this.lng).subscribe(suburb => {
			this.marker.lng = this.lng;
			this.marker.lat = this.lat;
			this.marker.label = suburb;
			this.profileForm.controls['location'].patchValue(suburb);
			this.profileForm.controls['location'].markAsDirty();
		});
	}

	private loadProfile() {
		this.profile = this.profileService.getProfile(this.profileId);
		this.createForm();
	}

	private createForm() {
		this.profileForm = this.fb.group({
			name: ['', Validators.required],
			email: ['', [Validators.required, Validators.email]],
			dateOfBirth: ['', Validators.required],
			location: ['', Validators.required]
		});

		if (this.profile) {
			this.profileForm.patchValue(this.profile);
			if (this.profile.geoLocation) {
				this.setMapAndMarker(this.profile.geoLocation.lat, this.profile.geoLocation.lng);
			}
		}
	}

	public mapClicked(mapClickedEvent: MapClickedEvent) {
		this.marker.lat = mapClickedEvent.coords.lat;
		this.marker.lng = mapClickedEvent.coords.lng;
		this.marker.draggable = true;
		this.geoCodingService.getSuburb(this.marker.lat, this.marker.lng).subscribe(suburb => {
			this.marker.label = suburb;
			const locationCtrl = this.profileForm.controls['location'];
			locationCtrl.patchValue(suburb);
			locationCtrl.markAsDirty();
		});
	}

	public onSubmit() {
		const profile = <Profile>this.profileForm.getRawValue();
		profile.geoLocation = <GeoLocation>{
			lat: this.marker.lat,
			lng: this.marker.lng
		};
		if (!this.profile) {
			this.profileService.addProfile(profile);
		} else {
			profile.id = this.profile.id;
			this.profileService.updateProfile(profile);
		}
		this.router.navigate(['/']);
	}

	public onCancel() {
		if (!this.profile) {
			this.router.navigate(['/']);
		} else {
			const result = this.profileService.deleteProfile(this.profile.id);
			if (result) {
				this.router.navigate(['/']);
			} else {
				window.alert('Error deleting profile');
			}
		}
	}
}

