import { Component, OnInit } from '@angular/core';
import { ProfileService } from '../-modules/core/services/profile.service';
import { Profile } from '../-modules/core/services/models/profile';
import { Router } from '@angular/router';

@Component({
	selector: 'app-profiles',
	templateUrl: './profiles.component.html',
	styleUrls: ['./profiles.component.scss']
})
export class ProfilesComponent implements OnInit {

	public profiles: Profile[];

	constructor(
		private profileService: ProfileService,
		private router: Router) {
	}

	ngOnInit() {
		this.profiles = this.profileService.allProfiles();
	}

	public editProfile(p: Profile) {
		const url = '/profile/' + p.id;
		this.router.navigate([url]);
	}

	public deleteProfile(p: Profile) {
		this.profileService.deleteProfile(p.id);
		this.profiles = this.profileService.allProfiles();
	}
}
