import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ProfileComponent } from './profile/profile.component';
import { ProfilesComponent } from './profiles/profiles.component';
import { ProfileGuard } from './-modules/core/services/profile.guard';

export let AppRoutes = [
	{
		path: '',
		component: ProfilesComponent
	},
	{
		path: 'profile/new',
		data: [{isNew: true}],
		component: ProfileComponent
	},
	{
		path: 'profile/:id',
		canActivate: [ProfileGuard],
		component: ProfileComponent
	}
];

@NgModule({
	imports: [
		RouterModule.forRoot(AppRoutes)
	],
	exports: [
		RouterModule
	]
})
export class AppRoutesModule {
}
